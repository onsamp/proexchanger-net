#!/bin/bash

## README
# Written by David Aizenberg; 2017;


function ubuntuBuild {
	sudo su -c 'echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list' # MongoDB Prep

	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6                                                # MongoDB Prep
	sudo apt-get -y update                                                                                            # Start with a fresh update

	# Create working dir
	mkdir /tmp/appinstalller
	cd /tmp/appinstalller

	# Create Node Deployment location
	sudo mkdir /opt/app1
	sudo chmod 777 -R /opt/app1

	# Required Packages
	echo "Installing core packages (Gathering Requirements)"
	sudo apt-get -y install python-software-properties jq libssl-dev git-core pkg-config build-essential curl gcc g++ openssl libreadline6 libreadline6-dev zlib1g zlib1g-dev libyaml-dev libsqlite3-0 libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison subversion traceroute

	# MongoDB Nginx Redis
	echo "Installing MongoDB and Redis"
	sudo apt-get -y install mongodb-10gen nginx redis-server

	# Gather Installation config:
	echo "Gathering config for key:" $1
	data=`curl https://obmen-box.com/api/v1/?method=public_get_config_for_license&key=$1`

	# Parse config:

	name=`echo $data | jq -r '.mongodb.user'`
	pass=`echo $data | jq -r '.mongodb.password'`
	db_name=`echo $data | jq -r '.mongodb.db_name'`
	version=`echo $data | jq -r '.node.version'`
	nginx_url=`echo $data | jq -r '.nginx.url'`
	nginx_name=`echo $data | jq -r '.nginx.path'`
	postfixStatus=`echo $data | jq -r '.postfix.status'`
	postfixDomain=`echo $data | jq -r '.postfix.domain'`


	echo $name "and" $version


	# Gather nginx conf and apply it
	echo "Configuring MongoDB and Nginx"
	wget $nginx_url
	sudo cp $nginx_name /etc/nginx/conf.d/
	sudo service nginx restart

	if [ $postfixStatus = true ]; then

		# Configure Docker
		curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
		sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
		sudo apt-get update
		sudo apt-get install -y docker-ce

	    docker run -p 2525:25 \
         -e maildomain=$postfixDomain -e smtp_user=$name:$pass \
         --name postfix -d catatnight/postfix
    fi

	# Configure mongodb
	sudo sed -i "s/bindIp: .*/bindIp: 0.0.0.0/" /etc/mongod.conf # Set mongo to local access only
	echo "use $db_name" > db.conf.js
	echo "use admin" >> db.conf.js
	 echo "db.createUser({user:\"$name\",pwd:\"$pass\", roles:[{role:\"root\",db:\"admin\"}]})" >> db.conf.js
	sudo service mongodb restart

	mongo admin -u admin -p admin < db.conf.js

	# NVM
	wget https://raw.github.com/creationix/nvm/master/install.sh
	chmod +x install.sh

	. install.sh

	source ~/.nvm/nvm.sh
	source ~/.profile
	source ~/.bash_profile
	source ~/.bashrc

	nvm install $version
	nvm use $version

	npm i -g pm2
	cd /opt/app1
	git clone https://github.com/jahio/hello-world-node-express.git
	cd hello-world-node-express
	npm i ; pm2 start app.js

	# Finishing Up
	echo "Cleaning Up"
	sudo apt-get -y autoremove
	sudo apt-get -y clean
	sudo apt-get -y autoclean
}

function debianBuild {
	echo "deb http://repo.mongodb.org/apt/debian wheezy/mongodb-org/3.4 main" | tee /etc/apt/sources.list.d/mongodb-org-3.4.list
	echo 'deb http://ftp.debian.org/debian/ wheezy-backports main contrib non-free' >> /etc/apt/sources.list

	apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6        # MongoDB Prep
	apt-get -y update                                                                                            # Start with a fresh update

	# Create working dir
	mkdir -p /tmp/appinstaller
	chmod 777 -R /tmp/appinstaller
	cd /tmp/appinstaller

	# Create Node Deployment location
	mkdir /opt/app1
	chmod 777 -R /opt/app1

	# Required Packages
	echo "Installing core packages (Gathering Requirements)"
	apt-get -y install software-properties-common jq libssl-dev git-core pkg-config build-essential curl gcc g++ openssl zlib1g zlib1g-dev libyaml-dev libsqlite3-0 libsqlite3-dev sqlite3 libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison subversion traceroute

	# MongoDB Nginx Redis
	echo "Installing MongoDB and Redis"
	apt-get -y --allow-unauthenticated install mongodb-org nginx redis-server

	# Gather Installation config:
	echo "Gathering config for key:" $1
	data=`curl https://proexchanger.net/api/v1/public_get_config_for_license/?key=$1`

	# Parse config:
	userid=`echo $data | jq -r '.user.username'`
	userpsk=`echo $data | jq -r '.user.password'`
	name=`echo $data | jq -r '.mongodb.user'`
	pass=`echo $data | jq -r '.mongodb.password'`
	db_name=`echo $data | jq -r '.mongodb.db_name'`
	version=`echo $data | jq -r '.node.version'`
	nginx_url=`echo $data | jq -r '.nginx.url'`
	nginx_name=`echo $data | jq -r '.nginx.path'`
	postfixStatus=`echo $data | jq -r '.postfix.status'`
	postfixDomain=`echo $data | jq -r '.postfix.domain'`

	echo $name "and" $version

	if [ $postfixStatus = true ]; then
		#statements
		# Docker
		apt-get install \
	    apt-transport-https \
	    ca-certificates \
	    curl \
		python-software-properties

		curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
		add-apt-repository \
	   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
	   $(lsb_release -cs) \
	   stable"
	   apt-get update
	   apt-get install docker-ce

	   docker run -p 2525:25 \
         -e maildomain=$postfixDomain -e smtp_user=$name:$pass \
         --name postfix -d catatnight/postfix
	fi


	# Gather nginx conf and apply it
	echo "Configuring MongoDB and Nginx"
	wget $nginx_url
	mkdir /etc/nginx/conf.d
	cp nginx.conf /etc/nginx/conf.d/$nginx_name
	service nginx restart

	%TODO
	# Configure mongodb
	sed -i "s/bindIp: .*/bindIp: 0.0.0.0/" /etc/mongod.conf # Set mongo to local access only
	echo "use $db_name" > db.conf.js
	echo "use admin" >> db.conf.js
	echo "db.createUser({user:\"$name\",pwd:\"$pass\", roles:[{role:\"root\",db:\"admin\"}]})" >> db.conf.js
	service mongodb restart

	su $userid -c "
		mongo admin -u admin -p admin < db.conf.js

		# NVM
		%TODO
		curl https://raw.github.com/creationix/nvm/master/install.sh >> install.sh
		chmod +x install.sh

		. install.sh

		source ~/.nvm/nvm.sh
		source ~/.profile
		source ~/.bash_profile
		source ~/.bashrc

		nvm install $version
		nvm use $version

		npm i -g pm2
		cd /opt/app1
		git clone https://github.com/jahio/hello-world-node-express.git
		cd hello-world-node-express
		npm i ; pm2 start app.js"

	# Finishing Up
	echo "Cleaning Up"
	apt-get -y autoremove
	apt-get -y clean
	apt-get -y autoclean
	rm -rf /tmp/appinstaller
}


echo "Hello, welcome to the {ВАШ СОФТ} installer: Adding repository:"

# Preparations
if [ "$(. /etc/os-release; echo $NAME)" = "Ubuntu" ]; then
	ubuntuBuild;
elif [ "$(. /etc/os-release; echo $ID)" = "debian" ]; then
	debianBuild;
else
	echo "We apologize but at the moment we will only support linux Debian or Ubuntu"
fi

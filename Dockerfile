FROM debian:jessie
MAINTAINER Support <support@obmen-box.com>
#FROM node:boron
#FROM nginx:1.12
#FROM mongo:3.5
RUN apt-get update --fix-missing && \
 apt-get install -y nano htop curl mc git && \
 apt-get install -y build-essential libssl-dev
RUN \
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6 && \
  echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.4 main" > /etc/apt/sources.list.d/mongodb-org-3.4.list && \
  apt-get update && \
  apt-get install -y mongodb-org && \
  rm -rf /var/lib/apt/lists/*
RUN mkdir -p /data/db

RUN rm /bin/sh && ln -s /bin/bash /bin/sh
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 8.4.0
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default



RUN echo "deb http://nginx.org/packages/debian/ jessie nginx" >> /etc/apt/sources.list.d/nginx.list
RUN apt-key adv --fetch-keys "http://nginx.org/keys/nginx_signing.key"

RUN apt-get update && apt-get -y dist-upgrade
RUN apt-get -y install nginx openssl ca-certificates

RUN rm -rf /etc/nginx/conf.d/*
RUN rm -rf /srv/www/*

ADD nginx/nginx.conf /etc/nginx/nginx.conf
ADD nginx/default.conf /etc/nginx/conf.d/default.conf
ADD nginx/index.html /srv/www/index.html

VOLUME ["/etc/nginx"]
VOLUME ["/srv/www"]

#CMD service nginx start
ENTRYPOINT ["/usr/sbin/nginx"]
#CMD ["-h"]




EXPOSE 80
EXPOSE 27017


CMD ["usr/bin/mongod", "--smallfiles"]

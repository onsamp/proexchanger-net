const db = require('../modules/db');
const error = require('../modules/error/api');
const config = require('../../bot_telegram/config');
const random = require('../modules/random');
const node_ssh = require('node-ssh');


module.exports = (API, redis) => {
    API.on('create_exchanger', true, (user, param, callback) => {
        new db.servers({
            domain: param.domain,
            email: param.email,
            key: (new Date().getTime()) + '-' + random.key(6, 6, 6, 8),
            ssh: {
                host: param.ssh_host,
                username: param.ssh_user,
                password: param.ssh_pass,
                port: param.ssh_port
            },
            user: {
                username: 'uu_' + random.str(7, 9),
                password: 'pu_' + random.str_(10, 15)
            },
            cloudflare: {
                login: param.email,
                pass: 'cf.' + random.str_(10, 15),
            },
            mongodb: {
                bindIp: '127.0.0.1',
                port: 27017,
                user: 'um_' + random.str(7, 8),
                password: 'pm_' + random.str_(10, 15),
                db_name: 'exchanger_' + random.count(100, 999)
            }
        }).save().then(function (res_lic) {
            console.log('!!! Create server LICENSE:', res_lic.key);
            console.log(res_lic._id);

            API.emit('public_cloudflare_user_create', user, {
                cloudflare_email: res_lic.cloudflare.login,
                cloudflare_pass: res_lic.cloudflare.pass
            }, function (err_cf, res_cf) {
                if (err_cf)
                    return console.error(null, {
                        success: false,
                        error: error.api('Request cloudflare_user_create error', 'server', err_cf, 0),
                    });
                if (res_cf.error)
                    return console.error(null, {
                        success: false,
                        error: res_cf.error,
                    });
                console.log('res', res_cf);

                db.servers.update({key: res_lic.key}, {
                    $set: {
                        "cloudflare.key": res_cf.response.user_key,
                        "cloudflare.api_key": res_cf.response.user_api_key,
                        "cloudflare.register": true
                    }
                }, {multi: true}).then(console.log);
            }, 'server');
            API.emit('public_sendgrid_whitelabel_domains', user, {
                cloudflare_email: res_lic.cloudflare.login,
                cloudflare_pass: res_lic.cloudflare.pass
            }, function (err_cf, res_cf) {
                if (err_cf)
                    return console.error(null, {
                        success: false,
                        error: error.api('Request cloudflare_user_create error', 'server', err_cf, 0),
                    });
                if (res_cf.error)
                    return console.error(null, {
                        success: false,
                        error: res_cf.error,
                    });
                console.log('res', res_cf);

                db.servers.update({key: res_lic.key}, {
                    $set: {
                        "cloudflare.key": res_cf.response.user_key,
                        "cloudflare.api_key": res_cf.response.user_api_key,
                        "cloudflare.register": true
                    }
                }, {multi: true}).then(console.log);
            }, 'server');
            let ssh = new node_ssh();
            ssh.connect({
                host: param.ssh_host,
                username: param.ssh_user,
                password: param.ssh_pass,
                port: param.ssh_port
            }).then(function () {
                console.log('[create_exchanger] ssh->then-> ' + param.ssh_host + ':' + param.ssh_port + ' Connected.');
                callback && callback(null, {
                    success: true,
                    ssh_server: param.ssh_host + ':' + param.ssh_port
                });
                ssh.exec('wget -qO- ifconfig.co', [], {
                    cwd: '/var/www',
                    stream: 'stdout',
                    options: {pty: true}
                }).then(function (result) {
                    db.servers.update({key: res_lic.key}, {
                        $push: {"dns": {type: 'a', name: '@', value: result}}
                    }, {safe: true, upsert: true}).then(console.log);
                });
                API.emit('public_sendgrid_whitelabel_domains', user, {
                    domain: res_lic.domain
                }, function (err_sg, res_sg) {
                    if (err_sg)
                        return console.error(null, {
                            success: false,
                            error: error.api('Request public_sendgrid_whitelabel_domains error', 'server', err_sg, 0),
                        });
                    if (res_sg.error)
                        return console.error(null, {
                            success: false,
                            error: res_sg.error,
                        });
                    for(let i in res_sg.dns) {
                        db.servers.update({key: res_lic.key}, {
                            $push: {"dns": {type: res_sg.dns[i].type, name: res_sg.dns[i].host, value: res_sg.dns[i].data}}
                        }, {safe: true, upsert: true}).then(console.log);
                    }
                },'server');
                ssh.exec('wget https://proexchanger.net/download/install.sh', [], {
                    cwd: '/var/www',
                    stream: 'stdout',
                    options: {pty: true}
                }).then(function (result) {
                    console.log('STDOUT: ' + result)
                })
            }).catch(function (err) {
                console.error('[create_exchanger] ssh->err->', err);
                callback && callback(null, {
                    success: false,
                    err: err,
                    ssh_server: param.ssh_host + ':' + param.ssh_port
                });
            });
        });
    }, {
        title: 'create exchanger',
        level: 3,// 0 public,1 user,2 admin,3 server
        description: 'Method reconfigures the settings of the mail server node js (be sure to restart the server ssh: pm2 restart all)',
        param: [
            {
                name: 'domain',
                type: "String",
                title: 'domain exchanger server example.com',
                necessarily: false
            }, {
                name: 'ssh_host',
                type: "String",
                title: 'server ssh 127.0.0.1',
                necessarily: false
            }, {
                name: 'ssh_user',
                type: "String",
                title: 'server ssh username',
                necessarily: false
            }, {
                name: 'ssh_pass',
                type: "String",
                title: 'server ssh password',
                necessarily: false
            }, {
                name: 'ssh_port',
                type: "Number",
                title: 'server ssh port (default: 22)',
                default: 22,
                necessarily: false
            }, {
                name: 'email',
                type: "String",
                title: 'creator email',
                necessarily: false
            }
        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'info', type: "string", title: 'msg info save', default: '`info`'},
            {name: 'error', type: "object", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });

    API.on('configure_server', true, (user, param, callback) => {
        if (param.key && param.key !== '' && typeof param.key === 'string' && param.value && param.value !== '') {
            if (!isNaN(param.value))
                config.set(param.key, +param.value);
            else if (param.value === 'true')
                config.set(param.key, true);
            else if (param.value === 'false')
                config.set(param.key, false);
            else if (param.value === 'null')
                config.set(param.key, null);
            else if (param.value === 'undefined')
                config.set(param.key, undefined);
            else if (typeof param.value === 'string')
                config.set(param.key, param.value);
            else {
                return callback && callback(null, {
                        error: error.api('Request param "param.value" typeof', 'param', {
                            pos: 'api/SERVER.js(configure_server):#1',
                            param: param
                        }, 0),
                        success: false
                    });
            }
        } else {
            return callback && callback(null, {
                    error: error.api('Request param "param.value" or "param.key" incorrect', 'param', {
                        pos: 'api/SERVER.js(configure_server):#2',
                        param: param
                    }, 0),
                    success: false
                });
        }


        callback && callback(null, {
            success: true,
            info: '!!! Please restart server !!!'
        })
    }, {
        title: 'Configure config.json server',
        level: 2,// 0 public,1 user,2 admin,3 server
        description: 'Method reconfigures the settings of the mail server node js (be sure to restart the server ssh: pm2 restart all)',
        param: [
            {
                name: 'key',
                type: "String",
                title: 'key:key2:key3',
                necessarily: false
            }, {
                name: 'value',
                type: "String",
                title: 'value conf',
                necessarily: false
            }
        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'info', type: "string", title: 'msg info save', default: '`info`'},
            {name: 'error', type: "object", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
};
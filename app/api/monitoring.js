/**
 * Created by bogdanmedvedev on 29.10.2017.
 */

const random = require('random-node');
const db = require('../modules/db');
const error = require('../modules/error/api');
const config = require('../../bot_telegram/config');
module.exports = (API, redis) => {
    API.on('get_list_monitoring_exchanger', true, (user, param, callback) => {
        db.exchanger_list.find({
            coment:{$nin:['']},
            xml:{$nin:['']},
            status:true
        },{
            name: 1,
            website: 1,
            xml: 1,
            link: 1
        }).then(function (exchanger_list) {
            return callback && callback(null, {
                result: exchanger_list,
                success: true
            });
        }).catch(function (err) {
            return callback && callback(null, {
                error: error.api(err.message, 'db', err, 5),
                success: false
            });
        });
    },{
        title: 'get exchanger list to monitoring',
        level: 0,
        description: 'get exchanger list to monitoring',
        param: [
        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
    API.on('add_to_list_monitoring', true, (user, param, callback) => {
        db.exchanger_list.find({
            website: param.website,
        }).count()
            .then(function (cnt) {
                if (cnt !== 0) {
                    return callback && callback(null, {
                            error: error.api('This "exchanger" already in use', 'param', {
                                pos: 'api/monitoring.js(add_to_list_monitoring):#5',
                                param: param
                            }, 1),
                            success: false
                        });
                }
                db.exchanger_list({
                    name: param.name,
                    website: param.website,
                    xml: param.xml,
                    link: param.link,
                    email: param.email,
                    login: param.login,
                    pass: param.pass,
                    bonus: param.bonus,
                    comment: param.comment
                }).save()
                    .then(function (document) {
                        // mail.send(param.email,
                        //     'Activate you account Abab.io',
                        //     'Hi ' + document.email + ', You have successfully created an Abab.io account. To complete the process, activate your account. https://abab.io/api/v1/?method=public_activate_email&email=' + document.email + '&hash=' + document.activate_hash + ' If you have any questions about this email, contact us. https://abab.io/support Regards, The Abab team',
                        //     'Hi ' + document.email + ', You have successfully created an Abab.io account. To complete the process, activate your account. <br>https://abab.io/api/v1/?method=public_activate_email&email=' + document.email + '&hash=' + document.activate_hash + ' <br><br>If you have any questions about this email, contact us. <br>https://abab.io/support <br><br>Regards, The Abab team');
                        return callback && callback(null, {
                                result: document,
                                success: true
                            });
                    })
                    .catch(function (err) {
                        return callback && callback(null, {
                                error: error.api(err.message, 'db', err, 5),
                                success: false
                            });
                    });

            })
            .catch(function (err) {
                return callback && callback(null, {
                        error: error.api(err.message, 'db', err, 5),
                        success: false
                    });
            });
    }, {
        title: 'Add to monitoring',
        level: 0,
        description: 'Add to monitoring exchanger',
        param: [
            {
                name: 'name',
                type: "string",
                title: 'exchanger name',
                necessarily: true
            },
            {
                name: 'website',
                type: "string",
                title: 'exchanger website url',
                necessarily: true
            },
            {
                name: 'xml',
                type: "string",
                title: 'exchanger url xml file',
                necessarily: true
            },
            {
                name: 'link',
                type: "string",
                title: 'exchanger link referal example .com/?ref=1234',
                necessarily: true
            },
            {
                name: 'email',
                type: "string",
                title: 'email for account',
                necessarily: true
            },
            {
                name: 'login',
                type: "string",
                title: 'login for account',
                necessarily: true
            },
            {
                name: 'pass',
                type: "string",
                title: 'pass to account',
                necessarily: true
            },
            {
                name: 'bonus',
                type: "number",
                title: 'monitoring bonus %',
                necessarily: true
            },
            {
                name: 'comment',
                type: "string",
                title: 'comment for admin',
                necessarily: true
            },

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
};
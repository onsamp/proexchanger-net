const random = require('random-node');
const passwordHash = require('password-hash');
const db = require('../modules/db');
const config = require('../../bot_telegram/config');
const request = require('../modules/request');
const error = require('../modules/error/api');

module.exports = (API, redis) => {
    config.set('cloudflare:partner:key', 'SEE partners.cloudflare.com', true);
    if (!config.get('cloudflare:partner:key') || config.get('cloudflare:partner:key') === 'SEE partners.cloudflare.com') return console.warn('[Warning] (cloudflare.js) config.json set param {"service_mail"}');

    API.proxy.cloudflare = function (name, param, callback) {

        let data_post = ''; // user_id=500150&
        param.act = name;
        param.host_key = config.get('cloudflare:partner:key');
        for (let i1 in param) {
            if (param.hasOwnProperty(i1))
                data_post += i1 + '=' + param[i1] + '&';
        }
        let option = {
            rejectUnauthorized: false,
            host: 'api.cloudflare.com',
            port: 443,
            path: '/host-gw.html',
            method: 'POST',
            formData: data_post,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        };
        request.getJSON(option, (statusCode, response) => {
            if (statusCode === 200 && Object.prototype.isPrototypeOf(response)) {
                callback && callback(null, response);
            } else {
                console.error('response', Object.prototype.isPrototypeOf(response), response);
                callback && callback('Error ethplorer API [' + '/en/api/' + ']. ( https://' + option.host + option.path + '?' + option.formData + ' ) Response code: ' + statusCode, null);
            }
        });
    };

    API.on('cloudflare_user_create', true, (user, param, callback) => {
        API.proxy.cloudflare('user_create', {
            cloudflare_email: param.cloudflare_email,
            cloudflare_pass: param.cloudflare_pass
        }, function (err, result) {

            if (err || result.result !== 'success') {
                return callback && callback(null, {
                        error: error.api('Request cloudflare api err', 'cloudflare', {
                            pos: 'api/cloudflare.js(cloudflare_user_create):#1',
                            param: param,
                            result: result,
                        }, 4),
                        success: false
                    });
            }
            callback && callback(null, {response: result.response, msg: result.msg, success: true})

        })
    }, {
        title: 'Create a Cloudflare account mapped to your user',
        description: 'This act parameter is used to create a new Cloudflare account on behalf of one of your users. If you know that your User already has an existing account with Cloudflare, use the "user_auth" operation. For your convenience, Cloudflare will automatically perform a "user_auth" if the Cloudflare account already exists.',
        param: [
            {
                name: 'cloudflare_email',
                type: "string",
                title: 'user Email',
                necessarily: true
            },
            {
                name: 'cloudflare_pass',
                type: "string",
                title: 'user Password',
                necessarily: true
            }

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'response.cloudflare_email', type: "string", title: 'data', default: ''},
            {name: 'response.user_key', type: "string", title: 'data', default: ''},
            {name: 'response.unique_id', type: "string", title: 'data', default: ''},
            {name: 'response.cloudflare_username', type: "string", title: 'data', default: ''},
            {name: 'response.user_api_key', type: "string", title: 'data', default: ''},
            {name: 'msg', type: "string", title: 'text result', default: ''},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'result', type: "string", title: '', default: 'success,error'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
};
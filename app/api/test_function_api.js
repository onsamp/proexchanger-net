const random = require('random-node');
const passwordHash = require('password-hash');
const db = require('../modules/db');
const config = require('../../bot_telegram/config');
const request = require('../modules/request');
const error = require('../modules/error/api');

module.exports = (API, redis) => {
    API.on('test_function_session_get', true, (user, param, callback) => {
        callback && callback(null,{session:user.session});
    }, {
        title: 'Registration user for email',
        level:0,
        description: 'Registration user for email method send verify message to "email" smtp ',
        param: [],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
    API.on('test_function_session_set', true, (user, param, callback) => {
        user.session.test_param = param.value;
        callback && callback(null,{session:user.session});
    }, {
        title: 'Registration user for email',
        description: 'Registration user for email method send verify message to "email" smtp ',
        param: [
            {
                name: 'value',
                type: "string",
                title: 'user Email',
                necessarily: true
            }

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
};
/**
 * Created by bogdanmedvedev on 15.09.17.
 */
const error = require('../modules/error/api');
const config = require('../../bot_telegram/config');

module.exports = (API, redis) => {

    API.on('get_config_for_license', true, (user, param, callback) => {
        let options ={
            server: {
                domain: config.get('domain'),

            },
            postfix:{
                status: false,
                domain: config.get('domain'),
                port: 2525,
                free_port: 25,
                path_dns_file_save:'/app_conf/dns.txt'
            },
            user: {
                username: 'exchanger_v2',
                password: 'password_user'
            },
            mongodb: {
                bindIp: '0.0.0.0',
                port: '27017',
                user: 'username',
                password: 'pass',
                db_name: 'db',
            },
            node: {
                version: "8.4.0",
            },

            nginx: {
                url: "http://"+config.get('domain')+"/download/nginx.conf",
                path: 'obmen-box.com.conf'
            }
        };
        callback && callback(null, options);
    }, {
        title: 'Get config for server installer',
        description: 'Get config for server installer',
        param: [
            {
                name: 'key',
                type: "string",
                title: 'user key',
                necessarily: true
            }

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
};
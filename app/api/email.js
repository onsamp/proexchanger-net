"use strict";

const random = require('random-node');
const db = require('../modules/db');
const error = require('../modules/error/api');
const nodemailer = require('nodemailer');
const config = require('../../bot_telegram/config');
require('ractive').DEBUG = false;
const ractiveRender = require('ractive-render');

const {SMTPServer} = require('smtp-server');
const {simpleParser} = require('mailparser');
const {EventEmitter} = require('events');
const portfinder = require('portfinder');
const request = require("request");

class MailServer extends EventEmitter {
    constructor(options, smtp_client) {
        super();
        this._smtpServer = new SMTPServer(Object.assign({}, options, {
            onData: this._onMailReceived.bind(this),
            authOptional: true,
        }));
        this._smtpServer.listen(options.port || 25, (err) => {
            if (err) {
                console.log(err);
                process.exit(1);
            }
            console.log('ServerMail is listening on port ' + options.port);
        });

    }

    _onMailReceived(stream, session, callback) {
        simpleParser(stream, (err, mail) => {
            if (err) return this.emit('error', err);
            this.emit('message', mail);
        });
        stream.on('end', callback);
    }
}
const sgMail = require('@sendgrid/mail');

module.exports = (API, redis) => {
    API.on('configure_email_server', true, (user, param, callback) => {
        if (param.host && param.host !== '')
            config.set('service_mail:host', param.host);
        if (!isNaN(param.port))
            config.set('service_mail:port', +param.port);
        if (!isNaN(param.secure))
            config.set('service_mail:secure', !(+param.secure));
        if (param.username && param.username !== '')
            config.set('service_mail:username', param.username);
        if (param.password && param.password !== '')
            config.set('service_mail:password', param.password);
        if (param.recived_port && param.recived_port !== '')
            config.set('service_mail:recived_port', param.recived_port);
        callback && callback(null, {
            success: true,
            conf: config.get('service_mail'),
            info: '!!! Please restart server !!!'
        })
    }, {
        title: 'Configure email smtp_server',
        level: 2,// 0 public,1 user,2 admin,3 server
        description: 'Method reconfigures the settings of the mail server node js (be sure to restart the server ssh: pm2 restart all)',
        param: [
            {
                name: 'host',
                type: "String",
                title: 'smtp.host',
                necessarily: false
            }, {
                name: 'username',
                type: "String",
                title: 'user <- @',
                necessarily: false
            }, {
                name: 'password',
                type: "String",
                title: '*******',
                necessarily: false
            },
            {
                name: 'port',
                type: "Number",
                title: 'Port smtp server:25,587',
                necessarily: false
            },
            {
                name: 'recived_port',
                type: "Number",
                title: 'Port recived mail (default:25)',
                necessarily: false
            },
            {
                name: 'secure',
                type: "Number",
                title: 'secure server smtp: 0 - false 1- true',
                necessarily: false
            }
        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'conf', type: "object", title: 'Get config mail', default: '12345'},
            {name: 'error', type: "object", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
    API.on('sendgrid_whitelabel_domains', true, (user, param, callback) => {
        let sub_domain = 'mail';
        let options = {
            method: 'POST',
            url: 'https://api.sendgrid.com/v3/whitelabel/domains',
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/json',
                'authorization': 'Bearer ' + config.get('service_mail:password')
            },
            body: {domain: param.domain, subdomain: sub_domain},
            json: true
        };

        request(options, function (err, response, body) {
            if (err) return callback && callback(null, {
                    success: false,
                    error: error.api('Error create domain in sendgrid.', 'sendgrid', err, 4)
                });
            if (body.errors) return callback && callback(null, {
                    success: false,
                    error: error.api('Error create domain in sendgrid. request err', 'sendgrid', body.errors, 4)
                });
            return callback && callback(null, {
                    success: true,
                    domain: sub_domain + '.' + param.domain,
                    dns: body.dns
                });
        });
    }, {
        title: 'Create a domain whitelabel.',
        level: 3,// 0 public,1 user,2 admin,3 server
        description: 'This endpoint allows you to create a whitelabel for one of your domains.\n\nIf you are creating a domain whitelabel that you would like a subuser to use, you have two options:\n\nUse the "username" parameter. This allows you to create a whitelabel on behalf of your subuser. This means the subuser is able to see and modify the created whitelabel.\nUse the Association workflow (see Associate Domain section). This allows you to assign a whitelabel created by the parent to a subuser. This means the subuser will default to the assigned whitelabel, but will not be able to see or modify that whitelabel. However, if the subuser creates their own whitelabel it will overwrite the assigned whitelabel.\nA domain whitelabel allows you to remove the “via” or “sent on behalf of” message that your recipients see when they read your emails. Whitelabeling a domain allows you to replace sendgrid.net with your personal sending domain. You will be required to create a subdomain so that SendGrid can generate the DNS records which you must give to your host provider. If you choose to use Automated Security, SendGrid will provide you with 3 CNAME records. If you turn Automated Security off, you will be given 2 TXT records and 1 MX record.\n\nFor more information on whitelabeling, please see our User Guide',
        param: [
            {
                name: 'domain',
                type: "String",
                title: 'domain.com',
                necessarily: false
            }
        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'conf', type: "object", title: 'Get config mail', default: '12345'},
            {name: 'error', type: "object", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });

    config.set('service_mail:host', 'SMTP_host', true, true); // 'smtp.yandex.ru'
    config.set('service_mail:port', 465, true, true);
    config.set('service_mail:recived_port', 25, true, true);
    config.set('service_mail:secure', true, true, true);// secure:true for port 465, secure:false for port 587
    config.set('service_mail:username', '', true, true);
    config.set('service_mail:password', '', true, true);
    if (!config.get('service_mail:port') || !config.get('service_mail:host') || config.get('service_mail:host') === 'SMTP_host' || config.get('service_mail:port') === 'SMTP_port' || !config.get('service_mail:username') || !config.get('service_mail:password') || config.get('service_mail:username') === '' || config.get('service_mail:password') === '') return console.warn('[Warning] (email.js) config.json set param {"service_mail"}');
    portfinder.getPorts(1, {port: config.get('service_mail:recived_port')}, function (err, ports) {
        if (ports[0] !== config.get('service_mail:recived_port')) return console.error('!! [ERROR] Email service dont start port ' + config.get('service_mail:recived_port') + ' used !!!');
        const mailServer = new MailServer({port: config.get('service_mail:recived_port')});

        mailServer.on('message', (msg) => {
            // console.dir(msg, {colors: true, depth: null});
            if (msg.from && msg.from.value && msg.from.value[0].address) {
                for (let i in msg.to.value) {
                    // if (msg.to.value[i].address.split('@')[1] === config.get('domain') || msg.to.value[i].address.split('@')[1] === 'obmen-box.com')
                    new db.email({
                        from: msg.from.value[0].address,
                        subject: msg.subject,
                        text: msg.text,
                        type: 'get',
                        attachments: msg.attachments,
                        textHtml: msg.textAsHtml,
                        to: msg.to.value[i].address,
                    }).save();
                    // else
                    console.warn('Recived to mail', msg.to.value[i].address)
                }
                // mailServer.sendMail('server@obmen-box.com', msg.from.value[0].address, 'Okay TEST', 'Hey, i\'ve received your message');
            }
        });
        API.on('get_one_email_message_by_id', true, (user, param, callback) => {
            db.email.findById(param.id, {
                _id: 1,
                text: 1,
                subject: 1,
                to: 1,
                from: 1,
                create_at: 1,
                read: 1
            }).then(function (m) {


                return callback && callback(null, {
                        success: true,
                        message: m
                    });


            }).catch(function (err) {
                return callback && callback(null, {
                        success: false,
                        error: error.api('Error get email message.', 'email', err, 4)
                    });

            });
        }, {
            title: 'Get one email message by id ',
            level: 2,// 0 public,1 user,2 admin,3 server
            description: 'To receive the list of the messages received by the server on the filter of the email message',
            param: [
                {
                    name: 'id',
                    type: "string",
                    title: '_id email',
                    necessarily: true
                }

            ],
            response: [
                {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
                {
                    name: 'message',
                    type: "array(object)",
                    title: '',
                    default: '[{_id: 1,text: 1,subject: 1,to: 1,from: 1,create_at: 1,read: 1}...]'
                },
                {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
            ]
        });


        API.on('get_email_messages', true, (user, param, callback) => {
            if (!param.limit || isNaN(param.limit)) param.limit = 30;
            if (+param.limit > 200) param.limit = 200;
            if (+param.limit < 1) param.limit = 1;
            if (!param.page || isNaN(param.page)) param.page = 1;
            if (+param.page < 1) param.page = 1;
            if (!param.type) param.type = 'get';
            if (param.type !== 'send' && param.type !== 'get' && param.type !== 'delete') param.type = 'get';
            let param_db = {
                type: param.type
            };
            if (param.type === 'get')
                param_db.to = param.email
            if (param.type === 'send')
                param_db.from = param.email
            if (param.type === 'delete')
                param_db.from = param.email

            db.email.find(param_db, {
                _id: 1,
                text: 1,
                subject: 1,
                to: 1,
                from: 1,
                create_at: 1,
                read: 1
            }).limit(+param.limit).skip((param.page - 1) * +param.limit).then(function (res) {
                let messages_result_db = res.map(function (m) {
                    return {
                        _id: m._id,
                        text: m.text.replace(/\s{2,}/g, ' ').slice(0, 150),
                        subject: m.subject.replace(/\s{2,}/g, ' ').slice(0, 100),
                        to: m.to,
                        name_from: "Name Sender".replace(/\s{2,}/g, ' ').slice(0, 50),
                        from: m.from,
                        create_at: m.create_at,
                        read: m.read
                    };
                })
                db.email.count({
                    type: param.type,
                    to: param.email
                }).then(function (count) {
                    return callback && callback(null, {
                            success: true,
                            messages: messages_result_db,
                            countcount: count,
                            page_select: +param.page,
                            page_count: Math.ceil(count / +param.limit)
                        });
                }).catch(function (err) {
                    return callback && callback(null, {
                            success: false,
                            error: error.api('Error get  count email messages.', 'email', err, 4)
                        });

                });
            }).catch(function (err) {
                return callback && callback(null, {
                        success: false,
                        error: error.api('Error get email messages.', 'email', err, 4)
                    });

            });


        }, {
            title: 'Get email messages',
            level: 2,// 0 public,1 user,2 admin,3 server
            description: 'To receive the list of the messages received by the server on the filter of the email message',
            param: [
                {
                    name: 'email',
                    type: "string",
                    title: 'from Email',
                    necessarily: true
                },
                {
                    name: 'limit',
                    type: "Number",
                    title: 'count emails one page',
                    necessarily: true
                },
                {
                    name: 'page',
                    type: "Number",
                    title: 'page count',
                    necessarily: true
                },
                {
                    name: 'type',
                    type: "string",
                    title: 'type messages (get,send,delete)',
                    necessarily: true
                }

            ],
            response: [
                {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
                {name: 'page_count', type: "int", title: 'Total page  (confirmed filter)', default: '12345'},
                {name: 'page_select', type: "int", title: 'Selected number page this', default: '12345'},
                {name: 'count', type: "int", title: 'Count messages (total && confirmed filter)', default: '12345'},
                {
                    name: 'messages',
                    type: "array(object)",
                    title: '',
                    default: '[{_id: 1,text: 1,subject: 1,to: 1,from: 1,create_at: 1,read: 1}...]'
                },
                {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
            ]
        });
    });

    API.on('confirm_register_create_email_message', true, (user, param, callback) => {
        let title = config.get('project_name') + ' - Activate account';
        ractiveRender.renderFile(
            'email-templates/confirm_email.html',
            {
                data: {
                    title: title,
                    param: param,
                    project: config.get('project_name'),
                    domain: config.get('domain')
                }
            },
            function (err, html) {
                if (err)
                    return callback && callback(null, {
                            error: error.api('Error confirm_register_create_email_message', 'email', err, 3),
                            success: false
                        });
                API.emit('send_email_message', {server: true}, {
                    email: param.email,
                    html: html,
                    title: title,
                    text: 'Verify your email ' + param.email + ': ' + config.get('domain') + param.hash + '',
                    error: err
                }, (err, resul) => {
                    if (err)
                        return callback && callback(null, {
                                error: error.api('Error send_email_message', 'email', err, 3),
                                success: false
                            });
                    callback && callback(null, {res_transfer: resul, success: true})
                });
            }
        );
    }, {
        title: 'Create template html register user',
        level: 3,// 0 public,1 user,2 admin,3 server
        description: 'method call only server ',
        param: [
            {
                name: 'lang',
                type: "string",
                title: 'user name',
                necessarily: true
            },
            {
                name: 'name',
                type: "string",
                title: 'user name',
                necessarily: true
            },
            {
                name: 'email',
                type: "string",
                title: 'user email',
                necessarily: true
            },
            {
                name: 'hash',
                type: "string",
                title: 'user hash string activate',
                necessarily: true
            }

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
    sgMail.setApiKey(config.get('service_mail:password'));
    API.on('send_email_message', true, (user, param, callback) => {
        sgMail.send({
            to: param.email,
            from: param.from,
            subject: param.title,
            text: param.text,
            html: param.html,
        }).then(function (res) {
            new db.email({
                from: param.from,
                subject: param.title,
                text: param.text,
                type: 'send',
                // messageId: info.messageId,
                attachments: [],
                textHtml: param.html,
                to: param.email,
            }).save().then(function (res) {
                callback && callback(null, {success: true, info: res});
            }).catch(function (err) {
                return callback && callback(err, {
                        success: false,
                        error: error.api('Error save email message.', 'email', err, 4)
                    });
            });
        }).catch(function (err) {
            return callback && callback(err, {
                    success: false,
                    error: error.api('Error send email message.', 'email', err, 4)
                });
        });
    }, {
        title: 'send_email_message',
        level: 2,// 0 public,1 user,2 admin,3 server
        description: 'method call only server ',
        param: [
            {
                name: 'from',
                type: "string",
                title: 'from Email',
                necessarily: true
            }, {
                name: 'email',
                type: "string",
                title: 'recipent Email',
                necessarily: true
            }, {
                name: 'title',
                type: "string",
                title: 'Email title',
                necessarily: true
            }, {
                name: 'text',
                type: "string",
                title: 'Email text',
                necessarily: true
            }, {
                name: 'html',
                type: "string",
                title: 'html template',
                necessarily: true
            }

        ],
        response: [
            {name: 'success', type: "string", title: 'Status request method', default: 'true, false'},
            {name: 'user_id', type: "string", title: 'user id', default: '12345'},
            {name: 'error', type: "string", title: '', default: 'Example: Error code'},
            {name: 'latency_ms', type: "int(11)", title: 'Request processing time in ms', default: '122'}
        ]
    });
}
;
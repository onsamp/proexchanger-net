/**
 * Created by bogdanmedvedev on 29.06.16.
 */
'use strict';
var config = require('../../../bot_telegram/config');
var error = require('../error');
var log = require('../log');
var geoip = require('../geoip');

var express = require('express'),
    app = express();
var bodyParser = require('body-parser'),
    session = require('express-session'),
    db = require('../db'),
    cookieParser = require('cookie-parser'),
    Cookies = require("cookies");
const MongoStore = require('connect-mongo')(session);

var compress = require('compression');
var helmet = require('helmet');
app.use(helmet());
app.use(compress({level: 9}));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// app.use(bodyParser.urlencoded({extended: false}));
//
var path_url = config.get('server:url:path');

app.use(config.get('server:url:path'), session({
    secret: config.get('server:session:secret'),
    name: config.get('server:session:name'),
    store: new MongoStore({
        mongooseConnection: db.mongoose.connection,
        // ttl: config.get('server:session:ttl_hours') * 60 * 60
    }),
    cookie: {
        maxAge: config.get('server:session:ttl_hours') * 60 * 60 * 1000 // hours
    },
    httpOnly: true,
    resave: true,
    saveUninitialized: true
}));
app.use(path_url, function (req, res, next) {
    req.initTimestamp = (new Date()).getTime();
    var IP = req.headers['x-forwarded-for'] || req.connection.remoteAddress || '0.0.0.0';
    IP = IP.replace('::ffff:', '');
    if (IP.split(',').length != 1) {
        IP = IP.split(',')[0]
    }
    if (IP == '::1') IP = '127.0.0.1';
    var infoIP = geoip(IP);

    req.session.lastUse = new Date();
    if (!req.session.first_ip)
        req.session.first_ip = IP;
    req.session.ip = IP;
    if (config.get('application:server:logs:express')) log.info('Express request: \n\t\tUrl: ' + req.protocol + '://' + req.get('Host') + req.url + '\n\t\tClient: ' + infoIP.ip + '/' + infoIP.counterCode);
    req.infoClient = infoIP;

    // req.infoClient.lang = req.params.lang;
    if (false == infoIP.success) {
        res.end('{"error":"You IP error","ip":"' + IP + '","support":"support@' + config.get('domain') + '"}');
        log.error('{express} client dont load page reason:[infoIP] : ', infoIP)
    } else {
        res.set('charset', 'utf8');
        next();
    }

});
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());

require('./api')(app, express);


var isPortTaken = function (port, fn) {
    var net = require('net');
    var tester = net.createServer()
        .once('error', function (err) {
            if (err.code != 'EADDRINUSE') return fn(err);
            fn(null, true)
        })
        .once('listening', function () {
            tester.once('close', function () {
                fn(null, false)
            }).close()
        })
        .listen(port)
};
var http_port = config.get('server:http:port');
var http = require('http').createServer(app).listen(http_port);
require('./web')(app, express);


module.exports.http = http;
module.exports.app = app;
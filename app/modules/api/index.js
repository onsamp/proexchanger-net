"use strict";
var config = require('../../../bot_telegram/config');
var request = require('../request');
var async = require("async");
var crypto = require('crypto');
var querystring = require('querystring');
var db = require('../db');
var error = require('../error/api.js');
var path = require('path');
var fs = require('fs');
var random = require('../random');

var API;
if (config.get('redis.status'))
    var redis = require('redis').createClient(config.get('redis.port'), config.get('redis.host'));

var controller = {};
API = {
    saveLog(name, err, user, param, json, type, request_id){
        new db.logsAPI({
            user_id: user._id,
            method: name,
            param: param,
            response: json,
            error: err,
            client: {
                user: user.user,
                ip: user.ip,
                session: user.session,
            },
            latency_ms: json.latency_ms,
            request_id: request_id,
            type_req: type
        }).save().catch(function (errdb) {
            console.error('[!!!] Error save log API', errdb, ' -0- ', name, err, user, param, json, type, request_id);
        })
    },
    on(name, _public, cb, docs){
        if (typeof _public == 'function') {
            docs = cb;
            cb = _public;
            _public = false
        }

        if (_public) name = 'public_' + name;
        if (!controller[name]) controller[name] = {};
        controller[name].fn = cb;
        controller[name].level = docs.level;
        if (_public)
            controller[name].level = 0;
        if (!docs.level && docs.level !== 0)
            controller[name].level = 3;

        if (!docs.hide) {
            docs.method = name;
            if (_public)
                docs.access = 1;
            else
                docs.access = 2;
            if (!docs.level) {
                if (_public)
                    docs.level = 0;
                else
                    docs.level = 1;
            }
            docs.code = cb.toString();
            setTimeout(function () {
                let add = true;
                for (let index in API.docs) {
                    if (API.docs[index].method === docs.method) {
                        API.docs[index] = docs;
                        add = false;
                    }
                }
                if (add) API.docs.push(docs);
            }, 1)
        }
    },
    emit(name, user, param, cb, type){
        var initTimestamp = (new Date()).getTime();
        var err = null;
        if (!param) param = {};
        if (!controller.hasOwnProperty(name)) {
            err = {
                message: '404 method not fount',
                error_type: 'param',
                object: {},
                level: 0,
                stack: (new Error()).stack
            };
            return cb && cb(err);
        }

        if (!name) {
            err = {error: '[api] method name null'};
            cb && cb(err);
        }
        async.parallel({
            user: function (callback) {
                callback(null, {status: false})
            },
            admin: function (callback) {
                callback(null, {status: false})
            }
        }, function (err, auth) {
            user.admin = auth.admin;
            user.user = auth.user;
            console.log(auth);
            if (controller[name].level === 1 && !user.user.status)
                return cb && cb(null, {
                        error: error.api('You are not authorized User', 'unauthorized', {
                            pos: 'modules/api/index.js(controller):#1',
                            param: param,
                            level: controller[name].level,
                            type: 'user'
                        }, 0, 401),
                        success: false
                    });

            if (controller[name].level === 2 && !user.admin.status)
                return cb && cb(null, {
                        error: error.api('You are not authorized Admin', 'unauthorized', {
                            pos: 'modules/api/index.js(controller):#2',
                            param: param,
                            level: controller[name].level,
                            type: 'admin'
                        }, 0, 401),
                        success: false
                    });


            if (controller[name].level === 3 && type !== 'server')
                return cb && cb(null, {
                        error: error.api('This method can not be used for REST-API', 'forbidden', {
                            pos: 'modules/api/index.js(controller):#3',
                            param: param,
                            level: controller[name].level,
                            type: 'server'
                        }, 0, 403),
                        success: false
                    });


            controller[name].fn(user, param, function (err, json) {
                if (err) {
                    console.error('API->emit(name, user, param, cb, type)->controller[name]->err:', err)
                }
                if (!json) {
                    json = {error: 'server error 500'};
                }
                json.latency_ms = (new Date()).getTime() - initTimestamp;
                if (!type) type = 'server';
                let request_id = new Date().getTime() + '-' + random.str(7, 7);
                API.saveLog(name, err, user, param, json, type, request_id);
                json.latency_ms = (new Date()).getTime() - initTimestamp;
                json.requestId = request_id;

                cb && cb(err, json);

            });
        });


    },
    docs: [], config: {
        secure: {
            http: '5po40445Qm-4FeESmF75a-Rc2J80YJ3z-UM28pPJ03u',
            api_server_key: 'o40445Qm-4FeESmF75a-Rc2J80YJ3z-UM28p',
        }

    },
    proxy: {},
    cache: {}
};
var chokidar = require('chokidar');

fs.readdir('./app/api', function (err, items) {
    for (var i = 0; i < items.length; i++) {
        console.log("API start:" + items[i]);
        require('../../api/' + items[i])(API, redis);
    }
});
// chokidar.watch(_path_root + 'app/api/*', {
//     persistent: true
// }).on('change', function (path) {
//     console.log("API update:"+path.split('/app/api/')[1]);
//
//     require(path)(API, redis);
//
// });
module.exports.controller = controller;
module.exports.API = API;
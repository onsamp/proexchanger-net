exports.api =function (message,type,stack,level,code) {

    return {message:message,error_type:type,object:stack,level:level,code:code,stack:(new Error()).stack}
};
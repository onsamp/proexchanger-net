"use strict";
const config = require('../../../bot_telegram/config');

config.set('aws-s3:region', 'us-west-2', true);
config.set('aws-s3:bucket', '', true);
config.set('aws-s3:key', '', true);
config.set('aws-s3:secret', '', true);

var aws_s3 = require('s3-image-uploader');
const uploader = new aws_s3({
    aws: {
        key: config.get('aws-s3:key'),
        secret: config.get('aws-s3:secret')
    },
    websocketServer: false,
    // websocketServerPort: 3004
});

var AWS = require('aws-sdk');
AWS.config.update({accessKeyId: config.get('aws-s3:key'), secretAccessKey: config.get('aws-s3:secret')});
function upload_file(file, buffer, file_name, cb) {
    let s3 = new AWS.S3();
    s3.putObject({
        Bucket: config.get('aws-s3:bucket'),
        Key: 'files/' + file_name,
        Body: buffer,
        ACL: 'public-read'
    }, function (err, res) {
        if (err) {
            return cb && cb(null, {
                    success: false,
                    err: err,
                    error: err.message
                });
        }
        cb && cb(null, {
            success: true,
            url: 'https://s3-' + config.get('aws-s3:region') + '.amazonaws.com/' + config.get('aws-s3:bucket') + '/' + 'files/' + file_name,
            attach_path: 'files/' + file_name,
            file: file
        });

    });

}
function upload_json(file_name, json, cb) {

    json = new Buffer.from(JSON.stringify(json));
    let s3 = new AWS.S3();
    s3.putObject({
        Bucket: config.get('aws-s3:bucket'),
        Key: 'json/' + file_name,
        Body: json,
        ACL: 'public-read'
    }, function (err, res) {
        if (err) {
            return cb && cb(null, {
                    success: false,
                    err: err,
                    error: err.message
                });
        }
        cb && cb(null, {
            success: true,
            url: 'https://s3-' + config.get('aws-s3:region') + '.amazonaws.com/' + config.get('aws-s3:bucket') + '/' + 'json/' + file_name,
            attach_path: 'json/' + file_name
        });

    });
}
const path = 'https://s3-' + config.get('aws-s3:region') + '.amazonaws.com/' + config.get('aws-s3:bucket')  + '/';

module.exports = {s3: aws_s3, path: path, upload_json: upload_json, upload_file: upload_file};
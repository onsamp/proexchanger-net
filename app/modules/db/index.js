const config = require('../../../bot_telegram/config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var mongo_db = mongoose.connect(config.get('database:mongodb_url'), {
    useMongoClient: true,
    server: {
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000
    }
    /* other options */
});
var db;

// docker exec -it some-mongo mongo admin
// docker run --name some-mongo -v /datadir:/data/db -d mongo
// db.createUser({user: "user", pwd: "pass", roles: [{ role: "readWrite", db: "db_name" }]})

var Schema = mongoose.Schema;
var schemas = {
    logsAPI: new Schema({

        user_id: {
            type: String,
            default: null
        },
        admin_id: {
            type: String,
            default: null
        },
        method: {
            type: String,
            default: null
        },
        param: {
            type: Schema.Types.Mixed,
        },
        response: {
            type: Schema.Types.Mixed,
        },
        error: {
            type: Schema.Types.Mixed,
            default: null,
        },
        client: {
            type: Schema.Types.Mixed,
        },
        latency_ms: {
            type: String,
            default: null
        },
        request_id: {
            type: String,
            default: null
        },

        type_req: {
            type: String,
            default: null
        },
        create_at: {
            type: Date,
            default: Date.now
        }
    }),
    users: new Schema({

        email: {
            type: String,
            default: null,
            index: true
        },
        phone: {
            type: String,
            default: null,
            index: true
        },
        name: {
            type: String,
            default: null
        },
        surname: {
            type: String,
            default: null
        },
        activate_hash: {
            type: String,
            default: null
        },
        password: {
            type: String,
            default: null
        },
        birthday: {
            type: String,
            default: null
        },
        address: {
            type: String,
            default: null
        },
        last_ip: {
            type: String,
            default: null
        },
        register_ip: {
            type: String,
            default: null
        },
        api: {
            key: {
                type: String,
                default: null
            },
            secret: {
                type: String,
                default: null
            },
            white_ip: {
                type: String,
                default: '*.*.*.*'
            },
            status: {
                type: Boolean,
                default: false
            }
        },
        settings: {
            type: Schema.Types.Mixed,
        },
        activate: {
            type: Boolean,
            default: false
        },
        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        },
        server_user: {
            type: Boolean,
            default: false
        }
    }),
    exchanger_list: new Schema({

        name: {
            type: String,
            default: null,
            index: true
        },
        website: {
            type: String,
            default: null,
            index: true
        },
        xml: {
            type: String,
            default: null
        },
        link: {
            type: String,
            default: null
        },
        email: {
            type: String,
            default: null
        },
        login: {
            type: String,
            default: null
        },
        pass: {
            type: String,
            default: null
        },
        bonus: {
            type: String,
            default: null
        },
        comment: {
            type: String,
            default: null
        },
        pro: {
            type: Boolean,
            default: false
        },
        api_status: {
            type: Boolean,
            default: false
        },
       status: {
            type: Boolean,
            default: true
        },

        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        }
    }),
    admin_users: new Schema({

        email: {
            type: String,
            default: null,
            index: true
        },


        name: {
            type: String,
            default: null
        },
        surname: {
            type: String,
            default: null
        },
        avatar: {
            type: String,
            default: null
        },
        password: {
            type: String,
            default: null
        },
        last_ip: {
            type: String,
            default: null
        },
        register_ip: {
            type: String,
            default: null
        },
        twofactory: {
            key: {
                type: String,
                default: null
            },
            secret: {
                type: String,
                default: null
            },
            type: {
                type: String,
                default: null
            },
            status: {
                type: Boolean,
                default: true
            }
        },
        api: {
            key: {
                type: String,
                default: null
            },
            secret: {
                type: String,
                default: null
            },
            white_ip: {
                type: String,
                default: '*.*.*.*'
            },
            status: {
                type: Boolean,
                default: false
            }
        },
        email_control: [],
        activate: {
            type: Boolean,
            default: false
        },
        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        },
        server_user: {
            type: Boolean,
            default: false
        }
    }),
    servers: new Schema({

        email: {
            type: String,
            default: null
        }, key: {
            type: String,
            default: null
        },
        domain: {
            type: String,
            default: null
        },
        dns: [{
            type: {
                type: String,
                default: null
            },
            name: {
                type: String,
                default: null
            },
            value: {
                type: String,
                default: null
            }
        }],
        ssh: {
            host: {
                type: String,
                default: null
            },
            username: {
                type: String,
                default: null
            },
            password: {
                type: String,
                default: null
            },
            port: {
                type: String,
                default: null
            }
        },
        cloudflare: {
            login: {
                type: String,
                default: null
            },
            pass: {
                type: String,
                default: null
            },
            key: {
                type: String,
                default: null
            },
            api_key: {
                type: String,
                default: null
            },
            register: {
                type: Boolean,
                default: false
            }
        },
        user: {
            username: {
                type: String,
                default: null
            },
            password: {
                type: String,
                default: null
            }
        },
        mongodb: {
            bindIp: {
                type: String,
                default: null
            },
            port: {
                type: Number,
                default: null
            },
            user: {
                type: String,
                default: null
            },
            password: {
                type: String,
                default: null
            },
            db_name: {
                type: String,
                default: null
            },
        },

    }),
    wallets: new Schema({

        user: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },

        active: {
            type: Boolean,
            default: true
        },
        type: {
            type: String,
            default: 'ETH'
        },
        wallet: {
            type: Schema.Types.Mixed,
            default: {},
        },
        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        }
    }),
    email: new Schema({
        messageId: {
            type: String,
            default: ''
        },
        from: {
            type: String,
            default: ''
        },
        to: {
            type: String,
            default: 'error@',
            index: true
        },
        subject: {
            type: String,
            default: ''
        },
        text: {
            type: String,
            default: ''
        },
        textHtml: {
            type: String,
            default: ''
        },


        type: { // get,send
            type: String,
            default: ''
        },
        read: {
            type: Boolean,
            default: false
        },
        read_at: {
            type: Date,
            default: null
        },
        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        },
        attachments: {
            type: Schema.Types.Mixed,
            default: []
        }

    }),
    tx: new Schema({

        user: {
            type: Schema.Types.ObjectId,
            ref: 'users'
        },

        currency: {
            type: String,
            default: 'USD',
        },
        tx: {
            type: Schema.Types.Mixed,
            default: {},
        },
        wallet: {
            type: String,
            default: '0x',
        },
        callback_url: {
            type: String,
            default: null,
        },
        nonce: {
            type: Number,
            default: 1
        },
        status: {
            type: Number,
            default: 0 //0- send tx to geth , 1 - wait confirm, 2 -confirm
        },
        create_at: {
            type: Date,
            default: Date.now
        },
        update_at: {
            type: Date,
            default: Date.now
        }
    }),

};

module.exports.open = mongo_db;
mongo_db.then(function (db_) {
    db = db_;
    db.on('error', function (err) {
        console.error('Connection error [Mongo DB]:', err.message);
    });

    for (let name in schemas) {
        if (schemas.hasOwnProperty(name)) {
            module.exports[name] = db.model(name, schemas[name]);

        }
    }


    module.exports.Schema = Schema;
    module.exports.mongoose = mongoose;
});
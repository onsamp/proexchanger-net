'use strict';
var jsonfile;
var Merchant;
var fs;
var config = require('../../config.json', 'dont-enclose');
var util;
var exchange;
var crypto = require('crypto');
module.exports = function (module, options) {
    util = module.util;
    fs = module.fs;
    jsonfile = module.jsonfile;
    exchange = module.exchange;
    Merchant = options.Merchant;
};

function getConfig() {
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
}

module.exports.config = getConfig;

module.exports.paySuccess = function (param, res) {
};
module.exports.payError = function (param, res) {
};
module.exports.payStatus = function (param, res) {
    var merchantConfig = getConfig();
    var API_Email = "";
    var API_SCI_Name = "";
    var API_Password = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Email")
            API_Email = el.account;
        if (el.type == "SCI_Name")
            API_SCI_Name = el.account;
        if (el.type == "Password")
            API_Password = el.account;
    });
    var stt_hesh = param.ac_transfer+':'+param.ac_start_date+':'+API_SCI_Name+':'+param.ac_src_wallet+':'+param.ac_dest_wallet+':'+param.ac_order_id+':'+param.ac_amount+':'+param.ac_merchant_currency+':'+API_Password;
    var hash = crypto.createHash('sha256')
        .update(stt_hesh)
        .digest('hex');
    if (hash == param.ac_hash) {
        exchange.getExchange(param.ac_order_id, "server", (rows)=>{
            var amount_1 = +(+rows[0].amount_1).toFixed(2);
            var type = rows[0].type;
            if (amount_1 <= +param.ac_amount && type.toUpperCase() == param.ac_merchant_currency.toUpperCase()) {
                res.writeHead(200);
                res.end();
                console.log("vse ok advcash.com");
                exchange.pay_order_exchange({exchangeID: param.ac_order_id, pay: 1}, 'ru', true);
            } else {
                console.error("ERROR merchant advcash.com /status/advcash.com/\n\t", param);
            }
        });
    } else {
        console.error("ERROR merchant advcash.com /status/advcash.com/\n\t", param);
    }

};
module.exports.pay = function (param, option, cb) {
    var merchantConfig = getConfig();
    var API_Email = "";
    var API_SCI_Name = "";
    var API_Password = "";

    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Email")
            API_Email = el.account;
        if (el.type == "SCI_Name")
            API_SCI_Name = el.account;
        if (el.type == "Password")
            API_Password = el.account;
    });

    if ((merchantConfig.XML).indexOf(param.XML) == -1) {
        console.log("this valut not supported merchant!");
        cb && cb(null, {action: 'error',title:"Ошибка", text:'При оплате произошла ошибка!', type:'error'});
    }else{
        var amount =+(+param.amount_1).toFixed(2);
        var stt_hesh = API_Email+':'+API_SCI_Name+':'+amount+':'+param.type.toUpperCase()+':'+API_Password+':'+param.id;
        var hash = crypto.createHash('sha256')
            .update(stt_hesh)
            .digest('hex');
        var data = {
            ac_account_email: API_Email,
            ac_sci_name: API_SCI_Name,
            ac_order_id: param.id,
            ac_amount: amount,
            ac_currency: param.type.toUpperCase(),
            ac_sign: hash,
            ac_comments:'Exchange ID: '+param.id+'. ('+config.domain+')',
            ac_success_url: 'https://' + config.domain + "/exchangerID_" + param.id + "_User_" + param.user + "_Route_" + param.valut1_id + "_" + param.valut2_id,
            ac_fail_url: 'https://' + config.domain + "/exchangerID_" + param.id + "_User_" + param.user + "_Route_" + param.valut1_id + "_" + param.valut2_id
        };

        var result = {action: 'redirect', metod: 'POST', data: data, url: 'https://wallet.advcash.com/sci/'}; //--для случая переадресации на страницу оплаты------
        //var result = {action: 'confirm_pay', result: 'yes/no', data:[], url: 'https://btcchange24.com/ru/exchangeid_546'}; //--для случая проверки оплаты по API как у BTC-E ---
        cb && cb(null, result);
    }
};
module.exports.change_config = function (param) {
    jsonfile.readFile(__dirname + '/config.json', function (err, data) {
        if (err) {
            console.error('[Error: 404](change_config) ' + err);
        } else {
            var conf = data;
            var set_c = {};
            param.type.forEach((el, i)=> {
                set_c[el] = param.pay[i];
            });
            conf.pay.forEach((el, i)=> {
                if (set_c.hasOwnProperty(el.type))
                    conf.pay[i].account = set_c[el.type];
            });
            jsonfile.writeFile(__dirname + '/config.json', conf, function (error) {
                if (error)
                    console.error("ERROR: writeFile config.json:" + error.message);
            });
        }
    });
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
};

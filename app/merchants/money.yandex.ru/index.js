'use strict';
var jsonfile;
var Merchant;
var fs;
var config = require('../../config.json', 'dont-enclose');
var util;
var exchange;
var crypto = require('crypto');
module.exports = function (module, options) {
    util = module.util;
    fs = module.fs;
    jsonfile = module.jsonfile;
    exchange = module.exchange;
    Merchant = options.Merchant;
};

function getConfig() {
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
}

module.exports.config = getConfig;

module.exports.paySuccess = function (param, res) {
};
module.exports.payError = function (param, res) {
};
module.exports.payStatus = function (param, res) {
    var merchantConfig = getConfig();
    var Passphrase = crypto.createHash('md5').update(merchantConfig.pay[0].account).digest("hex");
    var str = param.PAYMENT_ID + ':' + param.PAYEE_ACCOUNT + ':' + param.PAYMENT_AMOUNT + ':' + param.PAYMENT_UNITS + ':' + param.PAYMENT_BATCH_NUM + ':' + param.PAYER_ACCOUNT + ':' + Passphrase + ':' + param.TIMESTAMPGMT;
    str = str.toUpperCase();
    var v2hash = crypto.createHash('md5').update(str).digest("hex");
    v2hash = v2hash.toUpperCase();

    if (v2hash == param.V2_HASH) {
        exchange.getExchange(param.PAYMENT_ID, "server", (rows)=> {
            var amount_1 = +(+rows[0].amount_1).toFixed(2);
            var type = rows[0].type;
            if (amount_1 <= +param.PAYMENT_AMOUNT && type.toUpperCase() == param.PAYMENT_UNITS.toUpperCase()) {
                console.log(param);
                res.writeHead(200);
                res.end();
                console.log("vse ok");
                exchange.pay_order_exchange({exchangeID: param.PAYMENT_ID, pay: 1}, 'ru', true);
            } else {
                console.error("ERROR merchant perfectmoney.is /status/perfectmoney.is/\n\t", param);
            }
        });
    } else {
        console.error("ERROR merchant perfectmoney.is /status/perfectmoney.is/\n\t", param);
    }

};
module.exports.pay = function (param, option, cb) {
    var merchantConfig = getConfig();
    var api_Secret = "";
    var api_Wallet = "";

    merchantConfig.pay.forEach((el)=> {

        if (el.type == "Secret")
            api_Secret = el.account;
        if (el.type == "Wallet")
            api_Wallet = el.account;

    });

    if (api_Wallet == "") {
        console.log("this valut not supported merchant!");
        cb && cb(null, {
            action: 'error',
            title: "Ошибка",
            text: 'При оплате произошла ошибка! Не  указан  номер кашелька в  настройках мерчаинта',
            type: 'error'
        });
        return false;
    }
    if (api_Secret == "") {
        console.log("this valut not supported merchant!");
        cb && cb(null, {
            action: 'error',
            title: "Ошибка",
            text: 'При оплате произошла ошибка!Не  указано  Секретное слово в  настройках мерчаинта',
            type: 'error'
        });
        return false;
    }
    var data = {
        receiver: api_Wallet,
        "short-dest": 'Exchange ' + config.domain + ' ID:' + param.id,
        formcomment: 'Exchange ' + config.domain + ' ID:' + param.id,
        targets: 'Exchange ' + config.domain + ' ID:' + param.id,
        "quickpay-form": 'shop',
        label: param.id,
        paymentType: 'PC',
        sum: +(+param.amount_1).toFixed(2),
        // STATUS_URL: 'https://' + config.domain + '/merchant/status/perfectmoney.is',
        successURL: 'https://' + config.domain + "/exchangerID_" + param.id + "_User_" + param.user + "_Route_" + param.valut1_id + "_" + param.valut2_id
    };

    var result = {action: 'redirect', metod: 'POST', data: data, url: 'https://money.yandex.ru/quickpay/confirm.xml'}; //--для случая переадресации на страницу оплаты------
        
    //var result = {action: 'confirm_pay', result: 'yes/no', data:[], url: 'https://btcchange24.com/ru/exchangeid_546'}; //--для случая проверки оплаты по API как у BTC-E ---
    cb && cb(null, result);

};
module.exports.change_config = function (param) {
    jsonfile.readFile(__dirname + '/config.json', function (err, data) {
        if (err) {
            console.error('[Error: 404](change_config) ' + err);
        } else {
            var conf = data;
            var set_c = {};
            param.type.forEach((el, i)=> {
                set_c[el] = param.pay[i];
            });
            conf.pay.forEach((el, i)=> {
                if (set_c.hasOwnProperty(el.type))
                    conf.pay[i].account = set_c[el.type];
            });
            jsonfile.writeFile(__dirname + '/config.json', conf, function (error) {
                if (error)
                    console.error("ERROR: writeFile config.json:" + error.message);
            });
        }
    });
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
};

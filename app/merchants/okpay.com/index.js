'use strict';


// МЕРЧАИН НЕБЕЗОПАСНЫЙ  ПЕРЕНОСИТЬ НА НОВЫЙ  ОБМЕННИК  НЕЛЬЗЯ
// МЕРЧАИН НЕБЕЗОПАСНЫЙ  ПЕРЕНОСИТЬ НА НОВЫЙ  ОБМЕННИК  НЕЛЬЗЯ
// МЕРЧАИН НЕБЕЗОПАСНЫЙ  ПЕРЕНОСИТЬ НА НОВЫЙ  ОБМЕННИК  НЕЛЬЗЯ


var jsonfile;
var Merchant;
var request;
var querystring;
var fs;
var config = require('../../config.json', 'dont-enclose');
var util;
var exchange;
var crypto = require('crypto');
module.exports = function (module, options) {
    util = module.util;
    fs = module.fs;
    request = module.request.getJSON;
    querystring = module.querystring;
    jsonfile = module.jsonfile;
    exchange = module.exchange;
    Merchant = options.Merchant;
};

function getConfig() {
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
}

module.exports.config = getConfig;

module.exports.paySuccess = function (param, res) {
    var merchantConfig = getConfig();
    var api_Key = "";
    var api_Secret = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Key")
            api_Key = el.account;
        if (el.type == "Secret")
            api_Secret = el.account;
    });
    var ammount = +(+param.ok_txn_amount).toFixed(2);
    var stt_hesh = param.ok_item_1_custom_1_value + ':' + ammount + ':' + api_Secret;
    var hash = crypto.createHash('sha256')
        .update(stt_hesh)
        .digest('hex');
    if (hash == param.ok_item_1_custom_1_title) {
        exchange.getExchange(param.ok_item_1_custom_1_value, "server", (rows)=> {
            var amount_1 = +(+rows[0].amount_1).toFixed(2);
            var type = rows[0].type;
            if (amount_1 <= +param.ok_txn_amount && type.toUpperCase() == param.ok_txn_currency.toUpperCase() && param.ok_txn_status == 'completed' && rows[0].status == 2) {
                console.log(param);

                exchange.pay_order_exchange({exchangeID: param.ok_item_1_custom_1_value, pay: 1}, 'ru', true);
            }
        });
    }
    exchange.getExchange(param.ok_item_1_custom_1_value, "server", (rows)=> {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end("<html><head></head><body><script> location.href = 'https://" + config.domain + "/exchangerID_" + rows[0].id + "_User_" + rows[0].user + "_Route_" + rows[0].valut1_id + "_" + rows[0].valut2_id + "'</script></body></html>");
    })
};
module.exports.payError = function (param, res) {
    exchange.getExchange(param.ok_item_1_custom_1_value, "server", (rows)=> {
        res.writeHead(200, {"Content-Type": "text/html"});
        res.end("<html><head></head><body><script> location.href = 'https://" + config.domain + "/exchangerID_" + rows[0].id + "_User_" + rows[0].user + "_Route_" + rows[0].valut1_id + "_" + rows[0].valut2_id + "'</script></body></html>");
    })
};
module.exports.payStatus = function (param, res) {
    var merchantConfig = getConfig();
    var api_Key = "";
    var api_Secret = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Key")
            api_Key = el.account;
        if (el.type == "Secret")
            api_Secret = el.account;
    });
    var ammount = +(+param.ok_txn_amount).toFixed(2);
    var stt_hesh = param.ok_item_1_custom_1_value + ':' + ammount + ':' + api_Secret;
    var hash = crypto.createHash('sha256')
        .update(stt_hesh)
        .digest('hex');
    // param.ok_verify = true;
    var content = querystring.stringify(param);
    var options = {
        host: 'www.okpay.com',
        port: 443,
        path: "/ipn-verify.html",
        method: 'POST',
        formData: content,
        headers: {
            'content-length': content.length
        }
    };

    request(options, (code, result)=> {
        console.log('ipn', code, result);
    });
    if (hash == param.ok_item_1_custom_1_title) {
        exchange.getExchange(param.ok_item_1_custom_1_value, "server", (rows)=> {
            var amount_1 = +(+rows[0].amount_1).toFixed(2);
            var type = rows[0].type;
            if (amount_1 <= +param.ok_txn_amount && type.toUpperCase() == param.ok_txn_currency.toUpperCase() && param.ok_txn_status == 'completed' && rows[0].status == 2) {
                console.log(param);
                res.writeHead(200);
                res.end();
                exchange.pay_order_exchange({exchangeID: param.ok_item_1_custom_1_value, pay: 1}, 'ru', true);
            } else {
                res.writeHead(200);
                res.end();
                console.error("ERROR merchant okpay.com /status/okpay.com/\n\t", param);
            }
        });
    } else {
        res.writeHead(200);
        res.end();
        console.error("ERROR merchant okpay.com /status/okpay.com/\n\t", param);
    }

};
module.exports.pay = function (param, option, cb) {
    var merchantConfig = getConfig();
    var api_Key = "";
    var api_Secret = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Key")
            api_Key = el.account;
        if (el.type == "Secret")
            api_Secret = el.account;
    });

    if (api_Key == "" || api_Secret == "") {
        console.log("this valut not supported merchant!");
        cb && cb(null, {
            action: 'error',
            title: "Ошибка",
            text: 'При оплате произошла ошибка! Администратор сайта не настроил Merchant для оплаты данной валюты.',
            type: 'error'
        });
    } else {
        var ammount = +(+param.amount_1).toFixed(2);
        var stt_hesh = param.id + ':' + ammount + ':' + api_Secret;
        var hash = crypto.createHash('sha256')
            .update(stt_hesh)
            .digest('hex');
        console.log(stt_hesh);
        var data = {
            ok_receiver: api_Key,
            ok_item_1_name: 'ExchangeID:' + param.id + ' #' + config.domain,
            ok_item_1_price: ammount,
            ok_item_1_custom_1_title: hash,
            ok_item_1_custom_1_value: param.id,
            ok_currency: param.type.toUpperCase()
        };

        var result = {action: 'redirect', metod: 'POST', data: data, url: 'https://checkout.okpay.com/'}; //--для случая переадресации на страницу оплаты------
        //var result = {action: 'confirm_pay', result: 'yes/no', data:[], url: 'https://btcchange24.com/ru/exchangeid_546'}; //--для случая проверки оплаты по API как у BTC-E ---
        cb && cb(null, result);
    }
};
module.exports.change_config = function (param) {
    jsonfile.readFile(__dirname + '/config.json', function (err, data) {
        if (err) {
            console.error('[Error: 404](change_config) ' + err);
        } else {
            var conf = data;
            var set_c = {};
            param.type.forEach((el, i)=> {
                set_c[el] = param.pay[i];
            });
            conf.pay.forEach((el, i)=> {
                if (set_c.hasOwnProperty(el.type))
                    conf.pay[i].account = set_c[el.type];
            });
            jsonfile.writeFile(__dirname + '/config.json', conf, function (error) {
                if (error)
                    console.error("ERROR: writeFile config.json:" + error.message);
            });
        }
    });
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
};

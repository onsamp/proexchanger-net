'use strict';
var jsonfile;
var Merchant;
var fs;
var config = require('../../config.json', 'dont-enclose');
var util;
var exchange;
var crypto = require('crypto');
module.exports = function (module, options) {
    util = module.util;
    fs = module.fs;
    jsonfile = module.jsonfile;
    exchange = module.exchange;
    Merchant = options.Merchant;
};

function getConfig() {
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
}

module.exports.config = getConfig;

module.exports.paySuccess = function (param, res) {
    exchange.getExchange(param.m_orderid, "server", (rows)=>{
        res.redirect(307, 'https://' + config.domain + "/exchangerID_" + rows[0].id + "_User_" + rows[0].user + "_Route_" + rows[0].valut1_id + "_" + rows[0].valut2_id);
    })
};
module.exports.payError = function (param, res) {
    exchange.getExchange(param.m_orderid, "server", (rows)=>{
        res.redirect(307, 'https://' + config.domain + "/exchangerID_" + rows[0].id + "_User_" + rows[0].user + "_Route_" + rows[0].valut1_id + "_" + rows[0].valut2_id);
    })
};
module.exports.payStatus = function (param, res) {
    var merchantConfig = getConfig();
    var API_ID = "";
    var API_secret = "";
    var key_hesh = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "ID")
            API_ID = el.account;
        if (el.type == "secret")
            API_secret = el.account;
        if (el.type == "Password")
            key_hesh = el.account;
    });

    var stt_hesh = param.m_operation_id+':'+param.m_operation_ps+':'+param.m_operation_date+':'+param.m_operation_pay_date+':'+param.m_shop+':'+param.m_orderid+':'+param.m_amount+':'+param.m_curr+':'+param.m_desc+':'+param.m_status+':'+API_secret;
    var hash = crypto.createHash('sha256')
        .update(stt_hesh)
        .digest('hex');
    if (hash.toUpperCase() == param.m_sign.toUpperCase()) {
        exchange.getExchange(param.m_orderid, "server", (rows)=>{
            var amount_1 = (+rows[0].amount_1).toFixed(2);
            var type = rows[0].type;
            if (amount_1 <= +param.m_amount && type.toUpperCase() == param.m_curr.toUpperCase()) {
                res.writeHead(200);
                res.end(param.m_orderid+'|success');
                exchange.pay_order_exchange({exchangeID: param.m_orderid, pay: 1}, 'ru', true);
            } else {
                res.writeHead(500);
                res.end(param.m_orderid+'|error');
                console.error("ERROR merchant payeer.com /status/payeer.com/\n\t", param);
            }
        });
    } else {
        res.writeHead(500);
        res.end(param.m_orderid+'|error');
        console.error("ERROR merchant payeer.com /status/payeer.com/\n\t", param);
    }

};
module.exports.pay = function (param, option, cb) {
    var merchantConfig = getConfig();
    var API_ID = "";
    var API_secret = "";
    var key_hesh = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "ID")
            API_ID = el.account;
        if (el.type == "secret")
            API_secret = el.account;
        if (el.type == "Password")
            key_hesh = el.account;
    });

    if ((merchantConfig.XML).indexOf(param.XML) == -1) {
        console.log("this valut not supported merchant!");
        cb && cb(null, {action: 'error',title:"Ошибка", text:'При оплате произошла ошибка!', type:'error'});
    }else{
        var amount =(+param.amount_1).toFixed(2);
        var m_desc = new Buffer('ExchangeID:'+param.id+' #'+config.domain).toString('base64');

        var stt_hesh = API_ID+':'+param.id+':'+amount+':'+param.type.toUpperCase()+':'+m_desc+':'+API_secret;
        var hash = crypto.createHash('sha256')
            .update(stt_hesh)
            .digest('hex');

        var data = {
            m_shop: API_ID,
            m_orderid: param.id,
            m_amount: amount,
            m_curr: param.type.toUpperCase(),
            m_desc: m_desc,
            m_sign: hash.toUpperCase()
        };

        var result = {action: 'redirect', metod: 'post', data: data, url: 'https://payeer.com/merchant/'}; //--для случая переадресации на страницу оплаты------
        //var result = {action: 'confirm_pay', result: 'yes/no', data:[], url: 'https://btcchange24.com/ru/exchangeid_546'}; //--для случая проверки оплаты по API как у BTC-E ---
        cb && cb(null, result);
    }
};
module.exports.change_config = function (param) {
    jsonfile.readFile(__dirname + '/config.json', function (err, data) {
        if (err) {
            console.error('[Error: 404](change_config) ' + err);
        } else {
            var conf = data;
            var set_c = {};
            param.type.forEach((el, i)=> {
                set_c[el] = param.pay[i];
            });
            conf.pay.forEach((el, i)=> {
                if (set_c.hasOwnProperty(el.type))
                    conf.pay[i].account = set_c[el.type];
            });
            jsonfile.writeFile(__dirname + '/config.json', conf, function (error) {
                if (error)
                    console.error("ERROR: writeFile config.json:" + error.message);
            });
        }
    });
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
};

'use strict';
var jsonfile;
var Merchant;
var fs;
var config = require('../../config.json', 'dont-enclose');
var util;
var exchange;
var request;
var crypto = require('crypto');
var querystring;
var nonce = 0;
module.exports = function (module, options) {
    util = module.util;
    fs = module.fs;
    jsonfile = module.jsonfile;
    exchange = module.exchange;
    request = module.request.getJSON;
    querystring = module.querystring;
    Merchant = options.Merchant;
    nonce = +fs.readFileSync("./merchants/btc-e.com/nonce.json").toString();
    if (nonce < 1) nonce = 1;
};


function getConfig() {
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
}

module.exports.config = getConfig;

module.exports.paySuccess = function (param, res) {
};
module.exports.payError = function (param, res) {
};
module.exports.payStatus = function (param, res) {
};

function generatorNonce() {
    nonce++;
    if (4294966000 <= nonce) nonce = 10;
    fs.writeFile("./merchants/btc-e.com/nonce.json", nonce);
    return nonce;

}
module.exports.pay = function (param, option, cb) {
    var merchantConfig = getConfig();
    var api_Key = "";
    var api_Secret = "";
    merchantConfig.pay.forEach((el)=> {
        if (el.type == "Key")
            api_Key = el.account;
        if (el.type == "Secret")
            api_Secret = el.account;
    });
    if (api_Key == "" && api_Secret == "") {
        return console.error("Pless setting btc-e.com merchant");
    }

    var content = {
        nonce: generatorNonce(),
        method: 'RedeemCoupon',
        coupon: option['#code']
    };
    content = querystring.stringify(content);


    var sign = crypto
        .createHmac('sha512', new Buffer(api_Secret, 'utf8'))
        .update(new Buffer(content, 'utf8'))
        .digest('hex');

    var options = {
        host: 'btc-e.com',
        port: 443,
        path: "/tapi",
        method: 'POST',
        formData: content,
        headers: {
            Sign: sign,
            Key: api_Key,
            'content-type': 'application/x-www-form-urlencoded',
            'content-length': content.length
        }
    };
    request(options, (code, result)=> {
        if (code == 200 && typeof result == 'object' && result.success == 1) {

            var amount_1 = +(+param.amount_1).toFixed(param.tofixed);
            if (param.amount_1 > 500) amount_1 = amount_1 - 0.05;
            else amount_1 = amount_1 * 0.9999;  // розрешение погрешности для  избежание кофликтов с  проверкой сум на   JS  погрешность 0.01%

            var type = param.type;
            var type_req = result.return.couponCurrency;
            if (type_req.toUpperCase() == "RUR".toUpperCase()) type_req = 'RUB';
            if (amount_1 <= +(result.return.couponAmount).toFixed(param.tofixed) && type.toUpperCase() == type_req.toUpperCase()) {
                exchange.pay_order_exchange({exchangeID: param.id, pay: 1}, 'ru', true);
                cb && cb(null, {
                    action: 'error',
                    title: "Успешно",
                    text: 'Оплата принята! (' + result.return.transID + ')\n\n Сумма: ' + result.return.couponAmount + ' ' + type_req,
                    type: 'success',
                    nohidden: true
                });

            } else {
                console.error(amount_1, '<=', +(result.return.couponAmount).toFixed(param.tofixed), '&&', type.toUpperCase(), '==', type_req.toUpperCase());
                cb && cb(null, {
                    action: 'error',
                    title: "Ошибка",
                    text: 'Код принять сумма заявки не соответсвует коду:\nID обменна: ' + param.id + '.\n Принято: ' + result.return.couponAmount + ' ' + type_req + "\n\nНапишите в службу поддержки для решения данной проблеммы!",
                    type: 'error'
                });

            }
        } else {
            console.error(code, '==', 200, ' && ', typeof result, '== object && ', result.success, '==', 1);
            var err = 'Server response code:' + code;
            if (code == 200) err = result.error;
            switch (err) {
                case 'invalid coupon':
                    cb && cb(null, {
                        action: 'error',
                        title: "Ошибка",
                        text: 'Указаный код неверный!',
                        type: 'error',
                        nohidden: true
                    });
                    break;
                case 'empty coupon parameter':
                    cb && cb(null, {
                        action: 'error',
                        title: "Ошибка",
                        text: 'Вы  не указали BTC-e CODE!',
                        type: 'error',
                        nohidden: true
                    });
                    break;
                default:
                    cb && cb(null, {
                        action: 'error',
                        title: "Ошибка",
                        text: 'Серверная ошибка: ' + err,
                        type: 'error',
                        nohidden: true
                    });

                    console.error("ERROR [btc-e.com merchant]: statuscode(" + code, ') response:', result);
                    break
            }
        }
    });

    var result = {action: 'confirm_pay', result: 'yes/no', data: [], url: 'https://btcchange24.com/ru/exchangeid_546'}; //--для случая проверки оплаты по API как у BTC-E ---
    cb && cb(null, result);
};

module.exports.change_config = function (param) {
    jsonfile.readFile(__dirname + '/config.json', function (err, data) {
        if (err) {
            console.error('[Error: 404](change_config) ' + err);
        } else {
            var conf = data;
            var set_c = {};
            param.type.forEach((el, i)=> {
                set_c[el] = param.pay[i];
            });
            conf.pay.forEach((el, i)=> {
                if (set_c.hasOwnProperty(el.type))
                    conf.pay[i].account = set_c[el.type];
                if (conf.pay[i].account != set_c[el.type]) {
                    generatorNonce();
                    nonce = 9;
                }
            });
            jsonfile.writeFile(__dirname + '/config.json', conf, function (error) {
                if (error)
                    console.error("ERROR: writeFile config.json:" + error.message);
            });
        }
    });
    delete require.cache[require.resolve('./config.json')];
    return require('./config.json');
};